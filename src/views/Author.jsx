import React, { useEffect, useState } from "react";
import { Container, Form, Row, Col, Button, Table } from "react-bootstrap";
import {
  deleteAuthor,
  fetchAuthor,
  postAuthor,
  updateAuthor,
} from "../services/author_service";
import { uploadImage } from "../services/article_service";

export default function Author() {
  const [imageURL, setImageURL] = useState(
    "https://designshack.net/wp-content/uploads/placeholder-image.png"
  );
  const [imageFile, setImageFile] = useState(null);
  const [author, setAuthor] = useState([]);
  const [name, setName] = useState(null);
  const [email, setEmail] = useState(null);
  const [isUpdate, setIsUpdate] = useState(null);

  useEffect(() => {
    fetch();
  }, []);

  const fetch = async () => {
    let allAuthor = await fetchAuthor();
    setAuthor(allAuthor);
  };

  const onAdd = async (e) => {
    e.preventDefault();
    let newAuthor = { name, email };

    if (imageFile) {
      let url = await uploadImage(imageFile);
      newAuthor.image = url;
    }
    postAuthor(newAuthor).then((message) => {
      alert(message);
      fetch();
    });
    setImageFile(null);
    clear();
  };

  function clear(){
    document.getElementById("name").value = "";
    document.getElementById("email").value = "";
    document.getElementById("img").value = "";
    document.getElementById("image").src =
      "https://designshack.net/wp-content/uploads/placeholder-image.png";
  }

  function onDelete(id) {
    deleteAuthor(id);
    let afterDel = author.filter((item) => item._id !== id);
    setAuthor(afterDel);
  }

  const onEdit = (id, name, email, image) => {
    setIsUpdate(id)
    setName(name)
    setEmail(email)
    document.getElementById("name").value = name;
    document.getElementById("email").value = email;
    document.getElementById("image").src = image;
  };

  const onUpdate = async (e) => {
    e.preventDefault();
    let newAuthor = { name, email };

    if (imageFile) {
      let url = await uploadImage(imageFile);
      newAuthor.image = url;
    }

    updateAuthor(isUpdate, newAuthor).then(() => fetch());
    setIsUpdate(null);
    setImageFile(null);
    clear();
  };

  return (
    <div>
      <Container>
        <h1>Author</h1>
        <Row>
          <Col md={8}>
            <Form>
              <Form.Group>
                <Form.Label>Auth Name</Form.Label>
                <Form.Control
                  id="name"
                  type="text"
                  placeholder="Author Name"
                  onChange={(e) => setName(e.target.value)}
                />
                <Form.Text className="text-muted"></Form.Text>
              </Form.Group>

              <Form.Group>
                <Form.Label>Email</Form.Label>
                <Form.Control
                  id="email"
                  type="text"
                  placeholder="Email"
                  onChange={(e) => setEmail(e.target.value)}
                ></Form.Control>
              </Form.Group>
              <Button
                variant="primary"
                type="submit"
                onClick={isUpdate !== null ? onUpdate : onAdd}
              >
                {isUpdate !== null ? "Save" : "Add"}
              </Button>
            </Form>
          </Col>
          <Col md={4}>
            <img id="image" className="w-75" src={imageURL} />
            <Form>
              <Form.Group>
                <Form.File
                  id="img"
                  label="Choose Image"
                  onChange={(e) => {
                    let url = URL.createObjectURL(e.target.files[0]);
                    setImageFile(e.target.files[0]);
                    setImageURL(url);
                  }}
                />
              </Form.Group>
            </Form>
          </Col>
        </Row>

        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Email</th>
              <th>Image</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {author.map((item, index) => (
              <tr key={index}>
                <td>{index + 1}</td>
                <td>{item.name}</td>
                <td>{item.email}</td>
                <td>
                  <img src={item.image} alt="" width={200} height={120} />
                </td>
                <td>
                  <Button
                    size="sm"
                    variant="warning"
                    onClick={() =>
                      onEdit(item._id, item.name, item.email, item.image)
                    }
                  >
                    Edit
                  </Button>{" "}
                  <Button
                    size="sm"
                    variant="danger"
                    onClick={() => onDelete(item._id)}
                  >
                    Delete
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Container>
    </div>
  );
}
